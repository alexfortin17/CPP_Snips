/*
 * auto_decltype.cpp
 *
 *  Created on: Jun. 9, 2019
 *      Author: alfo
 */
#include <iostream>
using namespace std;

double f(){
	double x = 8.88;
	return x;
}

int auto_decltype(){

	cout << "\n### AUTO and DECLTYPE ###\n" << endl;
	cout << "The auto specifier will infer type at runtime" << endl;
	cout << "int a = 8; auto b = a; // b will be infered as an int at runtime" << endl;
	int a = 8;
	auto b = a;
	cout << "The result of typeid(b).name() is: " << typeid(b).name() << endl;

	cout << "Assuming we have a function f: double f{double x = 8.88; return x;}, we can delare an expression like:\n "
			"decltype(f()) y = f(); // The type of y will be the type returned by f() and the value the value returned by f()." << endl;
	decltype(f()) y = f();
	cout << "The result of typeid(y).name() is: " << typeid(y).name() << endl;
	cout << "The result of y is: " << y << endl;

	cout << "int i = 42; int *p = &i; int &r = i;" << endl;
	int i = 42;
	int *p = &i;
	int &r = i;
	cout << *p << typeid(*p).name() << endl;
	cout << r << typeid(r).name() << endl;
	cout << "decltype(*p) c; or decltype(r) d; will be a compilation error since *p on is own is a int&, not a plain int\n"
			"Since we have to initialize all references type, it fails\n"
			"When *p is used in an expression, it is automagically dereferenced and therefore, decltype(*p+0) c; will work." << endl;
	decltype(*p+0) c;
	cout << "The result of typeid(c).name() is: " << typeid(c).name() << endl;

	cout << "We can also force create a reference inside a decltype using double parantheses: \n"
			"decltype((i)) h = 8\n"
			"The only advantage is that the type being a ref, it will force you to assign a variable. \n"
			"The explanation is that decltype(i) simply gets the type of i while decltype((i)) EVALUATES THE EXPRESSION i \n"
			"A variable is an expression that can be the left side of the assignement. In the language implementation 'i' is a reference \n"
			"to the value '42'. Therefore, decltype((i)) return the int& type, not simply int" << endl;

	int j = 98;
	decltype(i) h = j;;

	return 0;
}






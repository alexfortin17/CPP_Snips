/*
 * arrays.cpp
 *
 *  Created on: Jun. 16, 2019
 *      Author: alfo
 */

#include <iostream>
using namespace std;

int arrays(){

	cout << "\n### ARRAYS ###\n" << endl;

	int x = 9;
	constexpr int y = 9; //become constant at COMPILE TIME

	//string bad[x];
	int *parr[y]; //array of size 9 containing pointers to ints
	cout << parr << endl;
	cout << *parr << endl;

	//initialize with values
	int a=1, b=2, c=3, d=4, e=5, f=6, g=7, h=8, i=9;
	int *sarr[y] = {&a,&b,&c,&d,&e,&f,&g,&h,&i};
	cout << *sarr[5] <<endl; //prints 6

	//looping through array
	cout << "looping through array" << endl;
	for (auto i : sarr){
		cout << i << ": " << *i << endl;
	}
	cout << "\n" <<endl;

	cout << "It is illefal to copy (or initialize) an array to another (like int a2[] = a or a2 = a)" << endl;
	cout << "Arrays if references are also impossible." << endl;
	cout << "Complex array declaration" <<endl;

	int *ptrs[10]; // ptrs is an array of 10 pointers to int
	cout << "int *ptrs[10]; // ptrs is an array of 10 pointers to int" << endl;

	int arr[10];
	int (*Parray) [10] = &arr; //Parray is a pointer to an array of size 10
	cout << "int (*Parray) [10] = &arr; //Parray is a pointer to an array of size 10" << endl;

	int (&arrRef) [10] = arr; //arrRef is a reference to an array of size 10
	cout << "int (&arrRef) [10] = arr; //arrRef is a reference to an array of size 10" << endl;

	int *(&arry)[10] = ptrs; //arry is a reference to an array of pointers to int of size 10
	cout << "int *(&arry)[10] = ptrs; //arry is a reference to an array of pointers to int of size 10" << endl;

	cout << "It's important to note that an array in C++ is simply a pointer to the 1st elem of the array" << endl;
	string nums[] = {"one", "two", "three"};
	string *p = nums;
	string *p2 = &nums[0];
	cout << "So: string *p = nums; is equivalent to: string *p2 = &nums[0];" << endl;
	bool bb = p == p2;
	cout << "p == p2 returns " << bb << endl;

	cout << "C++11 allows the use of begin() and end() on arrays" << endl;
	int ia[] = {0,1,2,3,4,5,6,7,8,9};
	int* beg = begin(ia);
	int* en = end(ia);

	//Pointer arithmetic
	int* tt = begin(ia);
	int* iaend = tt + 10; //point 1 past the last element of the array
	bool bb2 = iaend == end(ia);
	cout << bb2 << endl;

	cout << "\n### MULTIDIMENSIONAL ARRAYS ###\n" << endl;
	int ia2[3][4]; //array containing 3 arrays of size 4
	int ia3[3][4][5]; //array containing 3 arrays of size 4, each opf which contain an array of size 5

	//init
	int iax[3][4] = {
			{0,1,2,3},
			{4,5,6,7},
			{8,9,10,11}
		};
	//subscripting
	cout << iax[1][1] << endl; //returns 5
	//for range example
	int initt = 0;
	for (auto &x : iax){
		int initt2 = 0;
		for (auto y : x){
			cout << "The element at x: " << initt << " and y: " << initt2 << " is " << y << endl;
			++initt2;
		}
		++initt;
	}


	return 0;
}





/*
 * pointers.cpp
 *
 *  Created on: Jun. 9, 2019
 *      Author: alfo
 */

#include <iostream>
using namespace std;

int pointers(){
	cout << "\n### POINTERS ###\n" << endl;
	cout << "int a = 2; int *b = &a; //The & operator in an expression defines the getAddress function. \n"
			"Therefore, b holds the address of a." << endl;

	int a = 2;
	int *b = &a;

	cout << "To read the content of a, we must dereference the memory address using the * operator (cout << *b << endl;)" << endl;
	cout << "Result of *b is: " << *b << endl;

	cout << "Contrary to references, pointer can be null. In C++11, null pointer are defined: int *p1 = nullptr;" << endl;
	int *p1 = nullptr;
	cout << "reading nullptr gives: " << p1 << endl;
	cout << "dereferencing a null ptr crashes at run time. ALWAYS check if pointer is null before dereferencing" << endl;
	if (p1 != nullptr){
		cout << "the pointer is not Null" << endl;
	}

	cout << "There is also no limit to pointing to pointers. Here's a pointer of a pointer of a pointer:" << endl;
	cout << "int c = 8; int *d = &c; int **e = &d; int ***f = &e;" << endl;
	int c = 8;
	int *d = &c;
	int **e = &d;
	int ***f = &e;

	cout << "To get the value of the pointer of pointer of pointer f, we must dereference 3 times: ***f" << endl;
	cout << "Result of *f is: " << *f << endl;
	cout << "Result of **f is: " << **f << endl;
	cout << "Result of ***f is: " << ***f << endl;


	return 0;
}



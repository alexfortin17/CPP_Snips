/*
 * strings.cpp
 *
 *  Created on: Jun. 9, 2019
 *      Author: alfo
 */
#include <iostream>
using namespace std;

int strings(){
	cout << "\n### STRINGS ###\n" << endl;

	cout << "string str1 = \"Some string\";" << endl;
	string str1 = "Some string";
	cout << "string str2 = \"Some other string\";" << endl;
	string str2 = "Some other string";

	cout << "String concatenation: str1 + " " + str2" << endl;
	cout << str1 + " " + str2 <<endl;

	cout << "Its important to note that we cannot concatenate string literals (like 'hello' + 'world')\n"
			"To maintain compatibility with C, string literals do not have the type std:String" << endl;
	// cout << "Hello" + " World" << endl; //fails

	cout << "Length of a string: str.size()" << endl;
	cout << "The lenght of str1 is: " << str1.size() << endl;

	cout << "one can do random access on a string" << endl;
	cout << "str1[3] is: " << str1[3] << endl;

	cout << "We can also go through a string using a for: for (auto c: str1) { cout << c << endl; }" << endl;
	cout << "NOTE: Here, c is a COPY of the character found while looping. It therefore provides no access to the original chars" << endl;
	for (auto c: str1) {
		cout << c << endl; }

	cout << "We can also define the loop variable as a reference type in order no to copy the value.\n"
			"This also gives us access to the value if we ant to change it:\n"
			"Here, we will put all characters as uppercase\n"
			"for (auto &c: str1) { if (islower(c)) { c = toUpper(c); } } cout << str1 << endl;" << endl;
	for (auto c: str1) {
		if (islower(c)){
			c = toupper(c);
		}
	}
	cout << str1 << endl;

	return 0;
}


//============================================================================
// Name        : CPP_Snips.cpp
// Author      : Alex Fortin
// Version     :
// Copyright   : Whateves
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;
#include "initialization.h"
#include "type_conv.h"
#include "scoping.h"
#include "references.h"
#include "pointers.h"
#include "auto_decltype.h"
#include "structs.h"
#include "strings.h"
#include "vectors_cpp.h"
#include "iterators.h"
#include "arrays.h"

int main() {
	initialization();
	type_conv();
	scoping();
	references();
	pointers();
	auto_decltype();
	structs();
	strings();
	vect();
	iterators();
	arrays();


	cout << "\nProgram Successfully Terminated\n" << endl;

	return 0;
}

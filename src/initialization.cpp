#include <iostream>
using namespace std;

int initialization(){

	cout << "\n### INITIALIZATION ###\n" << endl;
	cout << "double p = 9.9" << endl;
	double p = 9.9;
	cout << "int y{p}; gives an error (or warning depending on compiler flag) "
			"since {} in initialization are not suppose to cast" << endl;
	cout << "int z(p); works fine since initialization using () will treuncate values if needed" << endl;
	int z(p);
	cout << "value of z " << z << endl;

	return 0;
}

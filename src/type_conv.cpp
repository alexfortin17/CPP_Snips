//============================================================================
// Name        : CPP_Snips.cpp
// Author      : Alex Fortin
// Version     :
// Copyright   : Whateves
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int type_conv() {

	cout << "\n### TYPE CONVERSION ###\n" << endl;

	cout << "bool b = 48 //this is true: " << endl;
	bool b = 48; //true
	cout << "value of b "<< b << endl;

	cout << "int x = b // x will be 1" << endl;
	int x = b; //b will be 1
	cout << "value of x " << x << endl;

	cout << "x = 3.14 // value will be 3 (truncated)" << endl;
	x = 3.14; //x will be 3
	cout << "value of x " << x << endl;

	return 0;
}

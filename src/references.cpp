/*
 * references.cpp
 *
 *  Created on: Jun. 9, 2019
 *      Author: alfo
 */
#include <iostream>
using namespace std;

int references(){
	cout << "\n### REFERENCES ###\n" << endl;
	cout << "References are similar to pointer in that thay store the memory address of another value\n."
			"They need not be dereferenced (* operator) like pointers. Poiter arithmetics is also a no-no" << endl;

	cout << "References can be treated as simple aliases" << endl;
	cout << "int x = 9; int &y = x" << endl;
	int x = 9;
	int &y = x;
	cout << " value of y: " << y << endl;

	cout << "Ref to const" << endl;
	cout << "const int z = 88; int &a = z; gives compilation error since underlying object is const" << endl;
	const int z = 88;
	// int &a = z; gives compilation error since underlying object is const
	cout << z << endl;

	cout << "const int a = 99; const int &b = a; give comilation arror since the ref is const and makes the reference read only." << endl;
	int a = 99;
	const int &b = a;
	// b = 77; give comilation arror since the ref is const and makes the reference read only.
	cout << b << endl;


	return 0;
}




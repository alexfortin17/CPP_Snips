/*
 * structs.cpp
 *
 *  Created on: Jun. 9, 2019
 *      Author: alfo
 */

#include <iostream>
using namespace std;

int structs(){

	cout << "\n### STRUCTS ###\n" << endl;

	cout << "struct Sales_data { string bookNo; unsigned units_sold = 0; double revenue = 0.0;};" << endl;

	struct Sales_data {
		string bookNo;
		unsigned units_sold = 0;
		double revenue = 0.0;
	};

	cout << "Sales_data data;" << endl;
	Sales_data data;
	cout << "data.bookNo = \"19524VV\";" << endl;
	data.bookNo = "19524VV";
	cout << "data.units_sold = 1000;" << endl;
	data.units_sold = 1000;
	cout << "double price = 99.99;" << endl;
	double price = 99.99;
	cout << "data.revenue = data.units_sold * price;" << endl;
	data.revenue = data.units_sold * price;

	cout << "total revenue: " << data.revenue << "$" << endl;


	return 0;
}



/*
 * vectors.cpp
 *
 *  Created on: Jun. 16, 2019
 *      Author: alfo
 */


#include <iostream>
#include <vector>
using namespace std;

int vect(){

	cout << "\n### VECTORS ###\n" << endl;

	//declaring empty vector
	vector<double> svec;

	//List initialize a vector
	vector<int> ivec = {1,2,3};
	cout << "Result of list initialization of vector: vector<int> ivec = {1,2,3};" << endl;
	for (auto i: ivec){
		cout << i << ' ';
	}
	cout << "\n" << endl;

	//Value initialization
	vector<int> ivec2(10);
	cout << "Result of value initialization of vector: vector<int> ivec2(10);" << endl;
	for (auto i: ivec2){
		cout << i << ' ';
	}
	cout << "\n" << endl;

	//Value initialization with non default value
	vector<int> ivec3(10, 1);
	cout << "Result of value initialization with non default value of vector: vector<int> ivec2(10, 1);" << endl;
	for (auto i: ivec3){
		cout << i << ' ';
	}
	cout << "\n" << endl;

	cout << "Copying on vector to another: vector<int> iveccopy = ivec;" << endl;
	vector<int> iveccopy = ivec;
	cout << "Modifying iveccopy: iveccopy[0] = 11" << endl;
	iveccopy[0] = 11;
	cout << "Content of iveccopy is now:" <<endl;
	for (auto i: iveccopy){
		cout << i << ' ';
	}
		cout << "\n" << endl;

	cout << "Content of ivec is still:" <<endl;
	for (auto i: ivec){
		cout << i << ' ';
	}
	cout << "\n" << endl;


	//Adding element to vector
	cout << "adding element to ivec with pushback: ivec.pushback(4)" << endl;
	ivec.push_back(4);
	cout << "Content of ivec is now:" <<endl;
	for (auto i: ivec){
		cout << i << ' ';
	}
	cout << "\n" << endl;

	//modifying vect
	cout << "Modifying vector through for range loops require reference to each element: for (auto &i : ivec) { if (i ==11) {i = 12;}}" << endl;
	for (auto &i : iveccopy){
		if (i == 11){
			i = 12;
		}
	}
	cout << "iveccopy is now" << endl;
	for (auto i: iveccopy){
		cout << i << ' ';
	}
	cout << "\n" << endl;






	return 0;
}

/*
 * iterators.cpp
 *
 *  Created on: Jun. 16, 2019
 *      Author: alfo
 */

#include <iostream>
#include <vector>
using namespace std;

int iterators(){

	cout << "\n### ITERATORS ###\n" << endl;

	string st = "Some string";

	cout << "iterating over a string(\"Some string\") and capitalizing letters: for (auto it = st.begin(); it != st.end(); ++it){ "
			"if (!isspace(*it)){ *it = toupper(*it); } }" << endl;
	for (auto it = st.begin(); it != st.end(); ++it){
		if (!isspace(*it)){
			*it = toupper(*it);
		}
	}
	cout << st << endl;



	return 0;
}


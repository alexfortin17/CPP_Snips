/*
 * scoping.cpp
 *
 *  Created on: Jun. 9, 2019
 *      Author: alfo
 */
#include <iostream>
using namespace std;

int outOfScope = 99;

int scoping(){
		cout << "\n### SCOPING ###\n" << endl;
		cout << "value of outOfScope variable (defined gloabally)" << endl;
		cout << outOfScope << endl;
		cout << "int outOfScope = 77;" << endl;
		int outOfScope = 77;
		cout << "value of outOfScope variable now" << endl;
		cout << outOfScope << endl;
		cout << "value of ::outOfScope variable now (empty namespace refers to globally defined vars)" << endl;
		cout << ::outOfScope << endl;//global scope is "nothing" and invoquing it here returns the global var

		return 0;
}

